package mtk.video.football

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.widget.VideoView
import androidx.recyclerview.widget.RecyclerView
import mtk.video.football.adapter.VideoAdapter
import mtk.video.football.model.Video
import mtk.video.football.view.DetailsActivity

class MainActivity : AppCompatActivity(), VideoAdapter.CallBackDownLoad {
    override fun onClick(url: String, name: String) {
        var intent : Intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra("url",url)
        startActivity(intent)
    }

    lateinit var rvVideos: RecyclerView
    lateinit var adapter: VideoAdapter
    lateinit var arrVideos: MutableList<Video>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rvVideos = findViewById(R.id.rvVideos) as RecyclerView
        adapter = VideoAdapter(this,arrVideos,this)
    }
}
