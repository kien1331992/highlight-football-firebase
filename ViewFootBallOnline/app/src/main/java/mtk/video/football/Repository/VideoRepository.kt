package mtk.video.football.Repository

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import mtk.video.football.model.Video

class VideoRepository private constructor(){

    companion object {
        @JvmStatic
        fun getInstance(): VideoRepository {
            return Holder.INSTANCE
        }

        private object Holder {
            val INSTANCE = VideoRepository()
        }


        public fun getAllVideo(completion: (MutableList<Video>) -> Unit, type: String) {
            var arrSongs: MutableList<Video> = mutableListOf()
            var db = FirebaseDatabase.getInstance().getReference(type)
            db.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    for (data in dataSnapshot.children) {
                        val data: Video? = data.getValue(Video::class.java)
                        if (data != null) {
                            arrSongs.add(data)
                        }
                    }
                    completion(arrSongs)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                }
            })
        }
    }
}