package mtk.video.football.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mtk.video.football.R
import mtk.video.football.model.Video

class VideoAdapter(ct: Context, arrVideos: List<Video>, _even: CallBackDownLoad) :
    RecyclerView.Adapter<VideoAdapter.ViewHolder>() {
    private lateinit var context: Context
    private lateinit var arrVideos: List<Video>
    private lateinit var even: CallBackDownLoad

    init {
        this.context = ct
        this.arrVideos = arrVideos
        this.even = _even
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(this.context).inflate(R.layout.item_video, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrVideos.size
    }

    fun setData(newData: List<Video>) {
        this.arrVideos = newData
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var video: Video = arrVideos.get(position)
        holder.tvTitle.text = video.title
        holder.tvDesciption.text = video.description
        holder.tvDate.text = video.date
//        Glide.with(context).load(song.linkAvatar).placeholder(R.mipmap.ic_placeholder)
//            .into(holder.imvAvatar)
//        holder.imvDownload.setOnClickListener(View.OnClickListener {
//            even.onClick(song.linkDownLoad, song.nameSong)
//        })
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public lateinit var tvTitle: TextView
        public lateinit var tvDesciption: TextView
        public lateinit var tvDate: TextView
        public lateinit var imvThumb: ImageView

        init {
            tvTitle = itemView.findViewById(R.id.tvTitle)
            tvDesciption = itemView.findViewById(R.id.tvContent)
            tvDate = itemView.findViewById(R.id.tvDate)
            imvThumb = itemView.findViewById(R.id.vThumb)
        }

    }

    interface CallBackDownLoad {
        fun onClick(url: String, name: String)
    }
}